from Crypto.Cipher import AES
import copy
import hashlib
from Crypto.Random import get_random_bytes
from bitarray import bitarray

print("CFB");

K = 17
R = 40
I = 6

message = bitarray(K * I)
m = message.copy()
register = bitarray(R)

print('message', message)
print('register', register)
cTxt = bitarray()

for i in range(I):
        output = bitarray()
        output.frombytes(hashlib.sha256(register.tobytes()).digest())
        xorRes = message[:K] ^ output[:K]
        message = message[K:]
        cTxt = cTxt + xorRes
        register = register[K:] + xorRes

print(cTxt)
cTxt[0] = cTxt[0] ^ 1
print(cTxt)
print(register)

recieved = bitarray()
for i in range(I):
        output = bitarray()
        output.frombytes(hashlib.sha256(register.tobytes()).digest())
        xorRes = cTxt[:K] ^ output[:K]
        register = register[K:] + cTxt[:K]
        cTxt = cTxt[K:]
        recieved = recieved + xorRes

print('recieved', recieved)

errors = 0
for i in range(0, len(recieved), K):
        x = recieved[i:i+K]
        y = m[i:i+K]
        if x != y:
           errors += 1
        print(recieved[i:i+K])
        print(m[i:i+K])

print("total errors:", errors)
#message = get_random_bytes(K * 2)
#m = copy.copy(message)
#
#register = bytearray(b"array")
#print("Register", len(register) * 8)
#print("K", 17)
#
#iterations = len(message) / K
#
#ciphertext = bytes()
# for i in range(int(iterations)):
#     # Ekoutput = Ek.encrypt(register)
#     Ekoutput = hashlib.sha256(register).digest()
#     xorResult = xorBytes(message[:K], Ekoutput[:K])
#     message = message[K:]
#     ciphertext = ciphertext + xorResult
#     register = register[K:] + xorResult
# 
# 
# register = bytearray(b"register")
# recieved = bytes()
# 
# ciphertext = bytearray(ciphertext)
# ciphertext[0] = ciphertext[0] ^ 1
# print(ciphertext[0])
# 
# for i in range(int(iterations)):
#     Ekoutput = hashlib.sha256(register).digest()
#     xorResult = xorBytes(ciphertext[:K], Ekoutput[:K])
# 
#     register = register[K:] + ciphertext[:K]
#     ciphertext = ciphertext[K:]
#     recieved = recieved + xorResult
# 
# print(len(m))
# print(len(recieved))
# errors = 0
# 
# for (ptxt, dtxt) in zip(m, recieved):
#     print(format(ptxt, '08b'))
#     print(format(dtxt, '08b'), '\n')
# 
# print("plaintext", list(m))
# print("decrypted", list(recieved))
# 
# for i in range(len(m)):
#     if m[i] != recieved[i]:
#         errors += 1
# 
# print(errors)
# print(round((errors - 1) / K + 1), "errors")
