import copy
import hashlib
from Crypto.Random import get_random_bytes
from bitarray import bitarray

print("OFB");

K = 17
R = 40
I = 6

message = bitarray(K * I)
m = message.copy()
register = bitarray(R)

print('message', message)
print('register', register)
cTxt = bitarray()

for i in range(I):
        output = bitarray()
        output.frombytes(hashlib.sha256(register.tobytes()).digest())
        xorRes = message[:K] ^ register[:K]
        message = message[K:]
        cTxt = cTxt + xorRes

print(cTxt)
cTxt[0] = cTxt[0] ^ 1
print(cTxt)
print(register)

recieved = bitarray()
for i in range(I):
        output = bitarray()
        output.frombytes(hashlib.sha256(register.tobytes()).digest())
        xorRes = cTxt[:K] ^ register[:K]
        cTxt = cTxt[K:]
        recieved = recieved + xorRes

print('recieved', recieved)

errors = 0
for i in range(0, len(recieved), K):
        x = recieved[i:i+K]
        y = m[i:i+K]
        if x != y:
           errors += 1
        print(recieved[i:i+K])
        print(m[i:i+K])

print("total errors:", errors)